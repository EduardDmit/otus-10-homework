import {MailBoxLayer} from './services/index'
const apiProvider = () =>({
    mailBoxLayer: () => new MailBoxLayer()
  });
  
export { apiProvider };
  