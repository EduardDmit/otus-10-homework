import fetch from 'node-fetch';
import { urls } from '../config/urls';

const MailBoxLayer = function MailBoxLayer() {
  this.check = async function check(params) {
    const url = new URL(`${urls.mailboxlayer}/check`)
    url.search = new URLSearchParams(params)
    const response = await fetch(url, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
      },
    });
    return response;
  };
};

export { MailBoxLayer };
