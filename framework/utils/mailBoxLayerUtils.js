class MailBoxParameters {
  constructor() {}

  get access_key() {
    return this.access_keyValue;
  }

  set access_key(value) {
    this.access_keyValue = value;
  }

  addAccessKey(value) {
    this.access_keyValue = value;
    return this;
  }

  get email() {
    return this.emailValue;
  }

  set email(value) {
    this.emailValue = value;
  }

  addEmail(value) {
    this.emailValue = value;
    return this;
  }

  get smtp() {
    return this.smtp;
  }

  set smtp(value) {
    this.smtp = value;
  }

  addSmtp(value) {
    this.smtp = value;
    return this;
  }

  get format() {
    return this.format;
  }

  set format(value) {
    this.format = value;
  }

  addFormat(value) {
    this.format = value;
    return this;
  }

  get callback() {
    return this.callback;
  }

  set callback(value) {
    this.callback = value;
  }

  addCallback(value) {
    this.callback = value;
    return this;
  }

  // TODO find another way to access existing field with getters
  getParametersAsObject() {
    return (properties = Object.getOwnPropertyNames(this).map(
      (name) => new Object({ [name.substring(0, name.indexOf('Value'))]: this[name] }),
    ));
  }
}

export { MailBoxParameters };
