import faker from "faker";

const MailBoxBuilder = function MailBoxBuilder() {
  this.addAccessKey = function (value) {
    this.access_key = value;
    return this;
  };
  this.addEmail = function(value) {
    this.email = value;
    return this;
  };
  this.addRandomEmail = function () {
    this.email = faker.internet.email();
    return this;
  };
  this.addSmtp = function (value) {
    this.smtp = value;
    return this;
  };
  this.addFormat = function (value) {
    this.format = value;
    return this;
  };
  this.addCallback = function (value) {
    this.callback = value;
    return this;
  };
  this.generate = function () {
    const fields = Object.getOwnPropertyNames(this);
    const data = {};
    fields.forEach((fieldName) => {
      if (this[fieldName] && typeof this[fieldName] !== "function") {
        data[fieldName] = this[fieldName];
      }
    });
    return data;
  };
};

export { MailBoxBuilder };
