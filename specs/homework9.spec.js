import { it, expect } from "@jest/globals";
import { MailBoxBuilder } from "../framework/builder/mailBoxBuilder";
import { user } from "../framework/config";
import { apiProvider } from "../framework/index";

describe("Home work 9", () => {
  it("Success check of valid email", async () => {
    const builder = new MailBoxBuilder();
    const params = builder
      .addAccessKey(user.mailboxlayer.access_key)
      .addRandomEmail()
      .generate();
    const response = await apiProvider().mailBoxLayer().check(params);
    expect(response.status).toEqual(200);
    const json = await response.json();
    expect(json.format_valid).toBeTruthy();
  });

  it.each`
    params                                                                              | code   | message
    ${new MailBoxBuilder().addRandomEmail().addAccessKey("InvalidAccesKey").generate()} | ${101} | ${"invalid_access_key"}
    ${new MailBoxBuilder().addAccessKey(user.mailboxlayer.access_key).generate()}       | ${210} | ${"no_email_address_supplied"}
  `(
    "Incorrect try to access API, and receive $code and $message",
    async ({ params, code, message }) => {
      const response = await apiProvider().mailBoxLayer().check(params);
      expect(response.status).toEqual(200);
      const json = await response.json();
      expect(json.success).toBeFalsy();
      expect(json.error.code).toBe(code);
      expect(json.error.type).toBe(message);
    }
  );

  it("Try to access witout access_key", async () => {
    const builder = new MailBoxBuilder();
    const params = builder.addRandomEmail().generate();
    const response = await apiProvider().mailBoxLayer().check(params);
    expect(response.status).toEqual(200);
    const json = await response.json();
    expect(json.success).toBeFalsy();
    expect(json.error.code).toBe(101);
    expect(json.error.type).toBe("missing_access_key");
  });
});
