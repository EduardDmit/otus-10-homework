module.exports = {
  testEnvironment: 'node',
  reporters: [
    'default',    
    "htmlreport4jest"
  ], //, 'jest-stare','jest-html-reporters'
  moduleFileExtensions: ['js', 'json'],
  transform: {
    '^.+\\.jsx?$': 'babel-jest',
  },
  testMatch: ['**/specs/*9.spec.*'],
  globals: {
    testTimeout: 50000,
  },
  verbose: true,
};
